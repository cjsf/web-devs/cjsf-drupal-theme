************************* WARNING ***********************************
THIS THEME IS BEING MODIFIED BY "CJSF RADIO, SIMON FRASER UNIVERSITY"

THE ORIGINAL NEXUS THEME NO LONGER RECEIVES UPDATES OR 
FIXES OF ANY KIND (INCLUDING SECURITY FIXES)
More info: https://git.drupalcode.org/project/nexus/-/tree/7.x-1.x?ref_type=heads

THIS THEME IS NO LONGER SUPPORTED BY DRUPAL.ORG
More info: https://www.drupal.org/project/nexus

*********************************************************************
The README text below is from the original project by devsaran.com
*********************************************************************

About Nexus
====================
Nexus is a Drupal 7 theme. The theme is not dependent on any 
core theme. Its very light weight for fast loading with modern look.
  Simple and clean design
  Drupal standards compliant
  Implementation of a JS Slideshow
  Multi-level drop-down menus
  Footer with 4 regions
  A total of 10 regions
  Compatible and tested on IE7, IE8, IE9+, Opera, Firefox, Chrome browsers

Browser compatibility:
=====================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x

Developed by
============
www.devsaran.com


Help and Support Us
=====================
Please consider a small donation 
Paypal ID : donation@devsaran.com
